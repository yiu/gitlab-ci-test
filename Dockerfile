FROM node

ARG DOWNLOAD_URL

WORKDIR /app

RUN echo ${DOWNLOAD_URL}

RUN echo $DOWNLOAD_URL

RUN curl -o /app/wordpress.tar.gz https://wordpress.org/latest.tar.gz

RUN curl -o /app/favicon.ico ${DOWNLOAD_URL}

RUN ls -las

CMD ["bash"]